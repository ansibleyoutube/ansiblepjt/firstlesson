FROM ubuntu:19.10

#Copia script para dentro do container
COPY build-image.sh .
COPY ansible-run.sh .
COPY inventory .
COPY main.yml .

#Da permissão para o script e o executa
RUN chmod +x build-image.sh
RUN chmod +x ansible-run.sh

#Executando o script para build da imagem
RUN ./build-image.sh
RUN ./ansible-run.sh

#Rodar com proxy
#docker build --build-arg http_proxy=http://10.58.97.134:8080 --build-arg https_proxy=http://10.58.97.134:8080 -t ubuntu-ansible:local .